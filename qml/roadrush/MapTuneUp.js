﻿
function echoCoordinates (valueX, valueY) {
    console.debug("first" + valueX);
    console.debug("second" + valueY);
};

var centerCoordinateX = 54.71095;
var centerCoordinateY = 55.9974;

var routeFoo = {
'0' : 'Россия, республика Башкортостан, Уфа, улица Красина, 15',
'1' : 'Россия, республика Башкортостан, Уфа, улица Гоголя, 56',
'2' : 'Россия, республика Башкортостан, Уфа, улица Ленина, 14',
'3' : 'Россия, республика Башкортостан, Уфа, Сочинская улица, 10',
'4' : 'Россия, республика Башкортостан, Уфа, улица Цюрупы, 12'
};

var xGeo = {
'1' : 54.71886,
'2' : 54.72886,
'3' : 54.73886,
'4' : 54.74886,
'5' : 54.75886
};

var yGeo = {
'1' : 55.98888,
'2' : 55.97888,
'3' : 55.96888,
'4' : 55.95888,
'5' : 55.94888
}


var tempRoutePoint01X = 54.71886;
var tempRoutePoint02X = 54.72886;
var tempRoutePoint03X = 54.73886;
var tempRoutePoint04X = 54.74886;
var tempRoutePoint05X = 54.75886;

var tempRoutePoint01Y = 55.98888;
var tempRoutePoint02Y = 55.97888;
var tempRoutePoint03Y = 55.96888;
var tempRoutePoint04Y = 55.95888;
var tempRoutePoint05Y = 55.94888;

var tempRoute = "[54.71886, 55.98888]";

